import pytest
import core


def test_there_are_tables_available():
    choice = 1
    tables = core.find_available(choice)

    assert len(tables) > 0

    # assert False, "Use core to find_available by choice, make sure there is at least one"


def test_table_can_be_booked():
    tables = core.all_tables()
    table = tables[0]

    assert table, 'Mesa'
    assert not table.is_booked, 'Libre'
    core.book_table(table.table_id), 'No more'
    assert table.is_booked

    # assert False, "Get a table (all tables), book it by ID, verify it is booked"


def test_cannot_book_a_nonexistant_table():
    with pytest.raises(core.EntityNotFoundError):
        # TODO: verify you cannot book a nonexistant table
        core.book_table('i')


def test_cannot_book_a_booked_table():
    with pytest.raises(core.TableUnavailableError):
        # TODO: verify you cannot book a table that is already booked
        table = core.find_available(1)[0]
        table.is_booked = True
        core.book_table(table.table_id)
